import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cart: [
      {
        name: "toto",
        price: 10,
      },
    ],
  },
  getters: {
    getTotalCart: (state) => {
      if (state.cart.length === 0) {
        return 0;
      } else {
        return state.cart
          .map((product) => product.price)
          .reduce((acc, price) => acc + price);
      }
    },
  },
  mutations: {},
  actions: {},
  modules: {},
});
